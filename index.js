const AWS = require('aws-sdk');
const { Client } = require('pg');


const TABLE_NAME = 'aws_accounts';
const DEFAULT_META = {
  supportType: 'Self Managed'
}


module.exports.handler = async function(event, context) {

  const pgclient = new Client({
    host: process.env.PGHOST,
    port: process.env.PGPORT,
    user: process.env.PGUSER,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD
  });

  console.log('connecting');
  try {
    await pgclient.connect();
  } catch (e) {
    console.log(e);
  }

  await event.Records.forEach(async (record) => {
    console.log('record', record);
    if (record.eventName === 'REMOVE') {
      await removeFromDataBase(record, pgclient);
    } else if (record.eventName === 'INSERT' || record.eventName === 'MODIFY') {
      await upsertIntoDataBase(record, pgclient);
    }
  });

  console.log('start query');
  try {
    const result = await pgclient.query(`SELECT * FROM ${TABLE_NAME}`);
    console.log('finished query');
    console.log(result);
  } catch (e) {
    console.log(e);
  }

  console.log('ending connection');
  pgclient.end();

}

async function removeFromDataBase(record, pgclient) {
  console.log(record.eventName);
  var keys = JSON.stringify(
    AWS.DynamoDB.Converter.unmarshall(record.dynamodb.Keys)
  );
  console.log(`deleting ${keys}`);
  try {
    const res = await pgclient.query(
      `DELETE FROM ${TABLE_NAME} WHERE id = ${keys.AccountCounterId}`
    );
    console.log('finished query');
    console.log(res);
  } catch (e) {
    console.log(e);
  }
}

async function upsertIntoDataBase(record, pgclient) {
  console.log(record.eventName);
  const obj = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
  var accountCounterId = obj.AccountCounterId;
  var newImg = JSON.stringify(obj);
  var defaultMeta = JSON.stringify(DEFAULT_META)
  console.log(`inserting item ${newImg} if not already present, else updating`);
  const query = `
    INSERT INTO ${TABLE_NAME} (id, data, meta) VALUES ('${accountCounterId}', '${newImg}', '${defaultMeta}')
    ON CONFLICT (id) DO UPDATE SET data = '${newImg}'
  `;
  console.log('query', query);
  try {
    const res = await pgclient.query(query);
    console.log('finished query');
    console.log(res);
  } catch (e) {
    console.log(e);
  }
}
